//
//  ArticleViewModel.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-20.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import Foundation


class ArticleViewModel: Identifiable {
    
    let uuid = UUID()
    
    let article: Article
    
    init(article: Article) {
        self.article = article
    }
    
    var title: String {
        return self.article.title
    }
    
    var author: String {
        return self.article.author ?? ""
    }
    
    var url:String {
        return self.article.url ?? ""
    }
    
    var description: String {
        return self.article.description ?? ""
    }
    
    var thumb: String {
        return self.article.urlToImage ?? ""
    }
    
    var source:String {
        return self.article.source?.name ?? ""
    }
    
    var publishedAt:String {
        return "◷ \(self.article.publishedAt.asDate.timeAgo()) ago"
    }
    
    
}
