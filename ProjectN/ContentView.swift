//
//  ContentView.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-20.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct ContentView : View {
    
    @ObservedObject var model = ArticleListView(articles:[])
    
    @ObservedObject var weatherModel = WeatherListView(weather:[])
    
    init(){
        UITableView.appearance().separatorStyle = .none
        UITableView.appearance().clipsToBounds = true
        UITableViewHeaderFooterView.appearance().tintColor = UIColor.clear
        UITableView.appearance().allowsSelection = false
        UITableViewCell.appearance().selectionStyle = .none
        UINavigationBar.appearance().backgroundColor = .white
    }
    
    func timer() -> String{
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        let dateString = formatter.string(from: Date())
        return dateString
    }
    
    var body: some View {
       
        NavigationView {
            List {
//                Section(header:
//                    Image("toronto")
//                        .frame(height:60)
//                ){
//                    Text("")
//                }
                Section(header:
                    
                    //Menu
                    ScrollView(.horizontal, showsIndicators: false,  content: {
                       HStack(alignment: .center) {
                               
                        ForEach(Menu.categories,id:\.self) { category in
                            
                                       Button(action: {
                                           self.model.load(withQ:"\(category)", true)
                                       },label:{
                                        
                                            Image(systemName: Menu.Item(rawValue:"\(category)")!.image)
                                            Text("\(category)")
                                        
                                       })
                                       .font(.system(size: 15, weight:.bold, design:.default))
                                       .padding()
                                       .background(Color.red)
                                       .cornerRadius(40)
                                       .foregroundColor(.white)
                                   }
                               }
                               .padding()
                           })
                           .frame(height: 70)
                           .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                           .background(Color.white)
                
                ){
                   
                  //Widgets
                  Text("Forecast \(self.timer())")
                        .font(.system(size: 14, weight:.semibold, design:.default))
                  ScrollView(.horizontal, showsIndicators: false, content: {
                       HStack {
                            ForEach( self.weatherModel.weather ) { data in
                                WeatherWidget(icon: data.icon,time: data.time, temperature: data.temperature)
                            }
                       }
                       .frame(height: 90)
                       .padding()
                   })
                       .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                       .frame(height: 90)
                       .background(Color.white)
                       .padding(.bottom,20)
                       .padding(.top,0)
                    
                    //Posts
                    ForEach(self.model.articles) { (article:ArticleViewModel) in
                        NavigationLink(destination: ArticleViewContent(article: article)) {
                           NewsListView(article: article)
                               .onAppear {
                                   self.model.loadArticles(currentArticle:article)
                                }
                           .frame(width:( ScreenSize.SCREEN_WIDTH - 36 ))
                           .padding(.trailing, 100)
                        }
                    }
                }

            }
            .navigationBarTitle(Text("Toronto News"))
            
        }
        .background(Color.white)
        .background(Image("toronto"))
        //.edgesIgnoringSafeArea(.bottom)
        
    }
    
}


struct WeatherWidget: View {
    
    @State var icon:Weather.Icon
    @State var time:Date
    @State var temperature:Float
    
    var body: some View {
        VStack(alignment: .center){
            
            Text("\(time.dateFormatting())")
                .font(.system(size: 12, weight:.medium, design:.default))
            
            Image(systemName: "\(icon.image)")
                .resizable()
                .scaledToFit()
                .foregroundColor(icon.color)
            
            Text("\(Int(temperature.fahrenheitToCelsius()))ºC")
                .font(.system(size: 16, weight:.bold, design:.default))
                .lineLimit(nil)
        }
        .frame(width:80,height:80)
        .background(Color.init(hex: 0xFFFFFF))
        .cornerRadius(22)
    }
    
}


struct ArticleImage: View {
    
    @State var article: ArticleViewModel
    
    var body: some View {
            
        WebImage(url: URL(string: "\(article.thumb)"))
            .onSuccess { image, cacheType in
                // Success
            }
            .resizable() // Resizable like SwiftUI.Image
            // Supports ViewBuilder as well
            .placeholder {
                Rectangle().foregroundColor(.gray)
            }
            .indicator(.activity) // Activity Indicator
            .aspectRatio(contentMode: .fill)
            .frame(maxWidth:( ScreenSize.SCREEN_WIDTH - 30 ))
            //.frame(height:200)
            .clipped()
            //.mask(Rectangle().padding(.top,-20))
        
        
    }
}

struct NewsListView: View {
    
    @State var article: ArticleViewModel
   
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
                if article.thumb != "" {
                    ArticleImage(article:article)
                }
                
                VStack(alignment: .leading){
                    
                    Text(article.source)
                       .foregroundColor(.secondary)
                       .font(.system(size: 16, weight:.bold, design:.default))
                    
                    Spacer().frame(height: 8)
                    
                    Text(article.title)
                        .font(.system(size: 22, weight:.black, design:.default))
                        .lineLimit(nil)
                        .multilineTextAlignment(.leading)
                    
                    Spacer().frame(height: 10)
                    
                    Text(article.publishedAt)
                        .foregroundColor(.secondary)
                        .font(.system(size: 13, weight:.medium, design:.default))
                    
                }
                //.frame(minWidth:self.size, maxWidth: self.size)
                .padding()
                //.padding(.top, 20)
            
        }
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color.black.opacity(0.2), radius: 9, x: 0, y: 10)
        
    }
}
