//
//  Articles.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-20.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//
import Foundation

struct NewsResponse: Codable {
    let articles: [Article]
}

struct Article: Codable {
    let title: String
    let description: String?
    let urlToImage: String?
    let url:String?
    let source:Source?
    let publishedAt:String
    let author:String?
}

struct Source: Codable {
    let id:String?
    let name:String?
}
