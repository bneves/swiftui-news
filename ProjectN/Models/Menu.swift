//
//  Menu.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-23.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import Foundation

struct Menu:Codable{
    
    let icon:String?
    
    static let categories:[String] = [
           "Business",
           "Entertainment",
           "Health",
           "Science",
           "Sports",
           "Technology"
       ]
    
}

extension Menu {

    enum Item: String, Codable {

        case Business       = "Business"
        case Entertainment  = "Entertainment"
        case Health         = "Health"
        case Science        = "Science"
        case Sports         = "Sports"
        case Technology     = "Technology"
        
        var image: String {
            switch self {
            case .Business:
                return "shield"
            case .Entertainment:
                return "guitars"
            case .Health:
                return "waveform.path.ecg"
            case .Science:
                return "lightbulb"
            case .Sports:
                return "sportscourt"
            case .Technology:
                return "gamecontroller"
            default:
                break
            }
        }

    }

}

