//
//  Weather.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-21.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import Foundation
import SwiftUI

struct Weather: Codable {
    let hourly:Hourly?
}

struct Hourly: Codable {
    let summary:String?
    let icon:String?
    let data:[HourlyData]?
}

struct HourlyData: Codable {
    let time:Date?
    let icon:String?
    let temperature:Float?
}

extension Weather {

    enum Icon: String, Codable {

        case clearDay = "clear-day"
        case clearNight = "clear-night"
        case rain = "rain"
        case snow = "snow"
        case sleet = "sleet"
        case wind = "wind"
        case fog = "fog"
        case cloudy = "cloudy"
        case partyCloudyDay = "partly-cloudy-day"
        case partyCloudyNight = "partly-cloudy-night"

        var image: String {
            switch self {
            case .clearDay:
                return "sun.max.fill"
            case .clearNight:
                return "moon.stars.fill"
            case .rain:
                return "cloud.rain.fill"
            case .snow:
                return "snow"
            case .sleet:
                return "cloud.sleet.fill"
            case .wind:
                return "wind"
            case .fog:
                return "cloud.fog.fill"
            case .cloudy:
                return "cloud.fill"
            case .partyCloudyDay:
                return "cloud.sun.fill"
            case .partyCloudyNight:
                return "cloud.moon.fill"
            }
        }
        
        var color: Color {
            switch self {
            case .clearDay:
                return Color(hex: 0xFFBC42, alpha: 1.0)
            case .clearNight:
                return Color(hex: 0xA9B4C2, alpha: 1.0)
            case .rain:
                return Color(hex: 0x85a1c7, alpha: 1.0)
            case .snow:
                return Color(hex: 0xEEF1EF, alpha: 1.0)
            case .sleet:
                return Color(hex: 0x68C5DB, alpha: 1.0)
            case .wind:
                return Color(hex: 0x006BA6, alpha: 1.0)
            case .fog:
                return Color(hex: 0x989FCE, alpha: 1.0)
            case .cloudy:
                return Color(hex: 0xC5CCCD, alpha: 1.0)
            case .partyCloudyDay:
                return Color(hex: 0x0496FF, alpha: 1.0)
            case .partyCloudyNight:
                return Color(hex: 0xEFEFF0, alpha: 1.0)
            }
        }

    }

}
