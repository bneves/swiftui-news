//
//  ArticleContent.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-20.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import SwiftUI
import WebKit


struct ArticleViewContent: View {
    
  @State var article: ArticleViewModel
  
  var body: some View {

    WebView(request:URLRequest(url: URL(string: "\(article.url)")!))
        .offset(x: 0, y: -44)
    
  }
  
}

struct WebView : UIViewRepresentable {
      
    let request: URLRequest
      
    func makeUIView(context: Context) -> WKWebView  {
        return WKWebView()
    }
      
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(request)
    }
      
}
