//
//  ArticlesListViewModel.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-20.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import APIManager

class ArticleListView: ObservableObject {
    
    let didChange = PassthroughSubject<ArticleListView,Never>()
    
    let api = "https://newsapi.org/v2/top-headlines?country=ca&apiKey=255b18f8b5e049fb9e6fbd70cdc2346e&category="
    var category = "business"
    
    var page:Int        = 0
    var isLoading:Bool  = false
    
    @Published var articles = [ArticleViewModel]() {
        didSet {
           didChange.send(self)
        }
    }
    
    init(articles: [Article]) {
        self.loadArticles()
    }
    
    //Veryfing for more articles
    func shouldLoadMoreArticles( currentArticle: ArticleViewModel? = nil ) -> Bool{
        
        if self.isLoading {
            return false
        }
        
        guard let currentArticle = currentArticle else{
            return true
        }
        
        guard let lastArticle = self.articles.last else{
            return true
        }
        
        return currentArticle.uuid == lastArticle.uuid
    }
    
    func load(withQ:String,_ newQuery:Bool = false){
        
        if newQuery {
            self.page = 1
            self.articles = [ArticleViewModel]()
            self.category = withQ
        }else{
            self.page += 1
        }
        
        let url = "\(self.api)\(self.category)&page=\(self.page)"
        print("url: \(url)")
        //Requesting API
        APIManager.shared.request(
            model: NewsResponse.self,
            url: url
        ){ (obj:Decodable?,status:HTTPStatusCode?,sucess:Bool) in

            if sucess {
                
                 switch status! {
                    case .ok:
                        let news:NewsResponse = obj as! NewsResponse
                        let articles:[Article] = news.articles
                        
                        DispatchQueue.main.async {
                            
                            if newQuery {
                                //
                                self.articles = articles.map(ArticleViewModel.init)
                                self.isLoading = false
                               
                            }else{
                                //Appeding for infinity scroll feature
                                for article in articles.map(ArticleViewModel.init) {
                                    self.articles.append(article)
                                }
                                //Increment for the next page
                                //self.page += 1
                                self.isLoading = false
                            }
                            
                        }
                        break
                    
                    default:
                        self.isLoading = false
                        break
                }
                
            }else{
                self.isLoading = false
                print("Error: \(obj ?? "")")
            }
        }
        
    }
    
    //Loading more articles
    func loadArticles( currentArticle: ArticleViewModel? = nil ){
        
        if !shouldLoadMoreArticles(currentArticle:currentArticle) {
            return
        }
        
        self.isLoading = true
        
        self.load(withQ:"\(self.category)")
        
    }
    
    
}
