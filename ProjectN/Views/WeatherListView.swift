//
//  WeatherListView.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-22.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import APIManager

class WeatherListView: ObservableObject {

    let didChange = PassthroughSubject<WeatherListView,Never>()

    let baseURL: String = "https://api.darksky.net/forecast/ab2ae14e323d74d51b04fdccfbc1d4cf/43.653225,-79.383186".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!

    @Published var weather = [WeatherViewModel]() {
       didSet {
          didChange.send(self)
       }
    }

    init(weather: [HourlyData]) {
       self.load()
    }

    func load(){
       
       //Requesting API
       APIManager.shared.request(
           model: Weather.self,
           url: self.baseURL
       ){ (obj:Decodable?,status:HTTPStatusCode?,sucess:Bool) in

           if sucess {
               
                switch status! {
                   case .ok:
                       let weather:Weather = obj as! Weather
                       let hour = weather.hourly!.data! as [HourlyData]
                       DispatchQueue.main.async {
                            self.weather = hour.map(WeatherViewModel.init)
                       }
                       break
                   
                   default:
                      
                       break
               }
               
           }else{
            print("Error: \(obj ?? "")")
           }
       }
       
    }


}
