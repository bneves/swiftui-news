//
//  WeatherListViewModel.swift
//  ProjectN
//
//  Created by Bruno Neves on 2019-12-21.
//  Copyright © 2019 www.brunoneves.com. All rights reserved.
//

import Foundation


class WeatherViewModel: Identifiable {
    
    let uuid = UUID()
    
    let weather: HourlyData
    
    init(weather: HourlyData) {
        self.weather = weather
    }
    
    var time: Date {
        return self.weather.time!
    }
    
    var icon: Weather.Icon {
        return Weather.Icon(rawValue: self.weather.icon!)!
    }
    
    var temperature: Float {
        return self.weather.temperature!
    }
    
}
